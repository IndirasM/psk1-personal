package lt.vu.usecases.cdi.dao;

import lt.vu.entities.Branch;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@ApplicationScoped
public class BranchDAO {
    @Inject
    private EntityManager em;

    public void create(Branch branch) {
        em.persist(branch);
    }

    public List<Branch> getAllBranches() {
        return em.createNamedQuery("Branch.findAll", Branch.class).getResultList();
    }
}
