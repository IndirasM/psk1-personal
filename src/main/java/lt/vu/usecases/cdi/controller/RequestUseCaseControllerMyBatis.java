package lt.vu.usecases.cdi.controller;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lt.vu.usecases.mybatis.dao.BranchMapper;
import lt.vu.usecases.mybatis.dao.EmployeeBranchMapper;
import lt.vu.usecases.mybatis.dao.EmployeeMapper;
import lt.vu.usecases.mybatis.model.Branch;
import lt.vu.usecases.mybatis.model.Employee;
import lt.vu.usecases.mybatis.model.EmployeeBranch;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Model
@Slf4j
public class RequestUseCaseControllerMyBatis {

    @Getter
    private Branch branch = new Branch();
    @Getter
    private Employee employee = new Employee();
    @Getter
    private List<Employee> allEmployees;

    @PostConstruct
    public void init() {
        loadAllEmployees();
    }

    @Inject
    private EmployeeMapper employeeMapper;
    @Inject
    private BranchMapper branchMapper;
    @Inject
    private EmployeeBranchMapper employeeBranchMapper;

    @Transactional
    public void createBranchEmployee() {
        branchMapper.insert(branch);
        employeeMapper.insert(employee);
        EmployeeBranch employeeBranch = new EmployeeBranch();
        employeeBranch.setBranchId(branch.getId());
        employeeBranch.setEmployeeId(employee.getId());
        employeeBranchMapper.insert(employeeBranch);
        log.info("Maybe OK...");
    }

    private void loadAllEmployees() {
        allEmployees = employeeMapper.selectAll();
    }
}
