package lt.vu.usecases.cdi.controller;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lt.vu.entities.Branch;
import lt.vu.entities.Employee;
import lt.vu.usecases.cdi.dao.BranchDAO;
import lt.vu.usecases.cdi.dao.EmployeeDAO;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Model
@Slf4j
public class RequestUseCaseControllerJPA {

    @Getter
    private Branch branch = new Branch();
    @Getter
    private Employee employee = new Employee();
    @Getter
    private List<Employee> allEmployees;

    @PostConstruct
    public void init() {
        loadAllEmployees();
    }

    @Inject
    private BranchDAO branchDAO;
    @Inject
    private EmployeeDAO employeeDAO;

    @Transactional
    public void createBranchEmployee() {
        employee.getBranchList().add(branch);
        branch.getEmployeeList().add(employee);
        branchDAO.create(branch);
        employeeDAO.create(employee);
        log.info("Maybe OK...");
    }

    private void loadAllEmployees() {
        allEmployees = employeeDAO.getAllEmployees();
    }
}
